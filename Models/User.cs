﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using MyRobots.CustomValidation;

namespace MyRobots.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string FirstName { get; set; }
        
        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string LastName { get; set; }
        
        [Required]
        [EmailAddress]
        [EmailUserUnique]
        [Column(TypeName = "nvarchar(100)")]
        public string Email { get; set; }
        
        [Required]
        [PasswordPropertyText]
        [Column(TypeName = "nvarchar(100)")]
        [JsonIgnore]
        public string Password { get; set; }
        
        [Required]
        [Column(TypeName = "int")]
        public int RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
    }
}