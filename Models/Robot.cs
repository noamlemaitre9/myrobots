﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace MyRobots.Models
{
    [Table("Robot")]
    public class Robot
    {
        public Robot()
        {
            this.CreatedAt = DateTime.Now;
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        [Column(TypeName = "float")]
        public float Price { get; set; }
        
        [Required]
        [Column(TypeName = "ntext")]
        public string Description { get; set; }

        [Column(TypeName = "int")]
        public int RobotTypeId { get; set; }
        [ForeignKey("RobotTypeId")]
        public RobotType RobotType { get; set; }

        [Column(TypeName = "int")]
        public int SellerId { get; set; }
        [ForeignKey("SellerId")]
        public User Seller { get; set; }

        [Column(TypeName = "int")]
        public int? BuyerId { get; set; }
        [ForeignKey("BuyerId")]
        public User Buyer { get; set; }
        
        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? SoldAt { get; set; }
    }
}