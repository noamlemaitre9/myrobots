import {Fragment, useEffect, useState} from 'react'
import {Popover, Transition} from '@headlessui/react'
import {MenuIcon, XIcon} from '@heroicons/react/outline'
import {BrowserRouter, Route} from "react-router-dom";
import Nav from "./components/Nav";
import Home from "./pages/Home";
import Login from "./pages/Login";
import RobotImage from "./components/RobotImage";
import Marketplace from "./pages/Marketplace";
import Register from "./pages/Register";
import CreateRobot from "./pages/CreateRobot";

export default function App() {
    const [logged, setLogged] = useState(false);
    const [userId, setId] = useState(0);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    useEffect(() => {
        (
            async () => {
                const response = await fetch('https://localhost:8001/api/User/info', {
                    headers: {'Content-Type': 'application/json'},
                    credentials: "include",
                });

                const content = await response.json();
                if (content.id != null) {
                    // Logged, set logged var to true and set user's properties
                    setLogged(true);
                    setId(content.id);
                    
                }
                else {
                    // Not logged
                }
            }
        )();
    });
    
    return (
        <BrowserRouter>
            <div className="relative bg-white overflow-hidden">
                <div className="max-w-7xl mx-auto">
                    <div
                        className="relative z-10 pb-8 sm:pb-16 md:pb-20 lg:w-full lg:pb-28 xl:pb-32">

                        <Nav logged={logged} setLogged={setLogged}/>

                        <main
                            className="mt-10 mx-auto max-w-7xl px-4">
                            <Route path={"/"} exact component={Home}/>
                            <Route path={"/login"} component={() => <Login setLogged={setLogged}/>} />
                            <Route path={"/register"} component={() => <Register setLogged={setLogged}/>} />
                            <Route path={"/marketplace"} component={() => <Marketplace logged={logged} />} />
                            <Route path={"/robot/create"} component={() => <CreateRobot logged={logged} userId={userId} />} />
                        </main>
                    </div>
                </div>
            </div>
            <Route path={"/"} exact component={RobotImage}/>
        </BrowserRouter>
    )
}
