﻿import React, {useEffect, useState} from 'react';
import {useQuery} from 'react-query';
import {Link} from "react-router-dom";
import {TiSpannerOutline, TiTree} from "react-icons/all";
import {list} from "postcss";
import {Robot} from "../components/Types";

const Marketplace = (props: { logged: boolean }) => {

    let addRobotButton: {} | null | undefined;
    let [robots, setRobots] = useState<Robot[]>([])

    // Get list of all robots on sale
    useEffect(() => {
        fetch('https://localhost:8001/api/robot', {
            headers: {'Content-Type': 'application/json'},
            credentials: "include",
        }).then(response => {
                response.json().then(data => {
                    setRobots(data);
                });
            }
        );
    }, []);

    // Display button to sell robot only if user is logged
    if (props.logged) {
        addRobotButton = (
            <>
                <Link
                    className="bg-white py-2 px-3 min-w-0 border border-gray-300 rounded-md shadow-sm text-sm leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    to={'/robot/create'}>
                    Sell my robot
                </Link>
            </>
        );
    }

    // Function to select icon depending on robotType
    const icon = (robotType: string) => {
        switch (robotType) {
            case 'Gardener':
                return <TiTree/>;
                break;
            case 'Mechanic':
                return <TiSpannerOutline/>;
                break
        }
    };

    return (
        <div className="flex flex-col">
            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="mb-5 ">
                        { addRobotButton }
                    </div>

                    <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
                        { robots?.map(robot => (
                            <div
                                className="bg-white border md:border-none shadow-xl md:shadow-2xl rounded-lg overflow-hidden my-6 mx-5 md:mt-10 md:mb-20">
                                <div className="p-4">
                                    <p className="text-3xl text-gray-900">{ robot.price } €</p>
                                    <div className="mt-2 flex border-gray-300 text-gray-700">
                                        <div className="flex-1 inline-flex items-center">
                                            {
                                                icon(robot.robotType.type)
                                            }
                                            <p className="ml-1">
                                                { robot.robotType.type }
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="px-4 py-2 border-t border-gray-300">
                                    <p className="mb-2 text-xl text-gray-900">
                                        Description:
                                    </p>
                                    <p className="text-gray-700">
                                        {robot.description}
                                    </p>
                                </div>
                                <div className="px-4 pt-3 pb-4 border-t border-gray-300 bg-gray-100">
                                    <div
                                        className="text-xs uppercase font-bold text-gray-600 tracking-wide">
                                        {'Sold by ' + robot.seller.firstName + ' ' + robot.seller.lastName}
                                    </div>
                                    <div
                                        className="text-xs uppercase font-bold text-gray-600 tracking-wide">
                                        {'At ' + robot.createdAt.replace('T', ', ').replace(/-/g, '/').replace(/[.](.*)/g, '')}
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Marketplace;