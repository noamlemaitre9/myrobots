﻿import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";

const Home = () => {    
    return (
        <>
            <div className="lg:max-w-2xl mt-10 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
                <div className="sm:text-center lg:text-left lg:max-w-2xl">
                    <h1 className="text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl">
                        <span className="block xl:inline">The future in front of</span>{' '}
                        <span className="block text-indigo-600 xl:inline">you</span>
                    </h1>
                    <p className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-20 md:text-xl lg:mx-0">
                        Don't waste your time anymore and buy a second-hand robot at an attractive price that will
                        do everything for you
                    </p>
                    <div className="mt-5 sm:mt-14 sm:flex sm:justify-center lg:justify-start">
                        <div className="rounded-md shadow">
                            <a
                                href="#"
                                className="w-full flex items-center justify-center px-8 py-3 transition duration-300 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10"
                            >
                                Create my account
                            </a>
                        </div>
                        <div className="mt-3 sm:mt-0 sm:ml-3">
                            <Link className={"w-full flex items-center justify-center px-8 py-3 transition duration-300 border border-transparent text-base font-medium rounded-md text-indigo-700 bg-indigo-100 hover:bg-indigo-200 md:py-4 md:text-lg md:px-10"} to="/login">Already have an account</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Home;