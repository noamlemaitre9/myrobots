﻿import {LockClosedIcon} from '@heroicons/react/solid'
import {SyntheticEvent, useState} from "react";
import { Redirect } from 'react-router-dom';

const Login = (props: {setLogged: (logged: boolean) => void }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);

    const [error, setError] = useState('');

    // Function to login user  
    const login = async (e: SyntheticEvent) => {
        e.preventDefault();

        const response = await fetch('https://localhost:8001/api/user/login', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            credentials: "include",
            body: JSON.stringify({
                email,
                password
            })
        });
        
        const content = await response.json();

        if (response.ok && content.success === true) {
            props.setLogged(true);
            setRedirect(true);
        } else {
            setError('Bad credentials...');
        }
    }

    // If user is logged, redirect is true and user is redirected to marketplace page
    if (redirect) {
        return <Redirect to={"/marketplace"}/>;
    }

    return (
        <div className="p-10 flex items-center justify-center filter drop-shadow-2xl px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Sign in to your account</h2>
                    <h4 className="mt-3 text-center text-xl font-bold text-gray-600">To buy your new robot</h4>
                </div>

                <div className="p-2 flex justify-center text-red-500">
                    <span>{ error }</span>
                </div>
                
                <form className="mt-8 space-y-6" method="POST" onSubmit={login}>
                    <div className="rounded-md shadow-sm -space-y-px">
                        <div>
                            <label htmlFor="email" className="sr-only">
                                Email
                            </label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                autoComplete="email"
                                required
                                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Email address"
                                onChange={e => setEmail(e.target.value)}
                            />
                        </div>
                        <div>
                            <label htmlFor="password" className="sr-only">
                                Password
                            </label>
                            <input
                                id="password"
                                name="password"
                                type="password"
                                autoComplete="current-password"
                                required
                                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Password"
                                onChange={e => setPassword(e.target.value)}
                            />
                        </div>
                    </div>

                    <div className="grid justify-items-center">
                        <div className="text-sm">
                            <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500">
                                Forgot your password?
                            </a>
                        </div>
                    </div>

                    <div>
                        <button
                            type="submit"
                            className="group relative w-full flex justify-center mt-20 py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        >
                            <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                                <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                                                aria-hidden="true"/>
                            </span>
                            Sign in
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Login;