﻿import {SyntheticEvent, useState} from "react";
import {Link, Redirect} from 'react-router-dom';
import AsyncSelect from 'react-select/async';

const CreateRobot = (props: { logged: boolean, userId: number }) => {
    const axios = require('axios');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');
    const [robotTypeId, setRobotTypeId] = useState('');
    const sellerId = props.userId;

    const [redirect, setRedirect] = useState(false);

    const [error, setError] = useState('');

    const [robotTypes, setRobotTypes] = useState([]);

    // Set robotType var when new select value is selected 
    const handleRobotTypeChange = (selectedOption: any) => {
        setRobotTypeId(selectedOption.value);
    };

    // Function to get all robot types
    const getRobotTypes = async () => {
        const robotTypesRequest = await axios.get('https://localhost:8001/api/robottype')
        const robotTypes = robotTypesRequest.data

        const options = robotTypes.map((robotType: { id: any; type: any; }) => ({
            "value" : robotType.id,
            "label" : robotType.type
        }));

        console.log(options);
        
        return options;
    };
    
    // Function to store new robot
    const storeRobot = async (e: SyntheticEvent) => {
        e.preventDefault();

        const response = await fetch('https://localhost:8001/api/robot', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            credentials: "include",
            body: JSON.stringify({
                price,
                description,
                robotTypeId,
                sellerId
            })
        });

        const content = await response.json();

        if (response.ok) {
            setRedirect(true);
        } else if (content.errors != null && content.errors.Email != null) {
            setError(content.errors.Email);
        } else {
            setError('An error as occured...');
        }
    }

    if (redirect) {
        return <Redirect to={"/marketplace"}/>;
    }
    
    if (!props.logged) {
        return <Redirect to={"/marketplace"}/>;
    }

    return (
        <div className="mt-10 sm:mt-16">
            <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                    <div className="px-4 sm:px-0">
                        <h3 className="text-lg font-medium leading-6 text-gray-900">Robot Information</h3>
                        <p className="mt-1 text-sm text-gray-600">Describe your robot here to sell it</p>
                    </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                    <form method="POST" onSubmit={storeRobot}>
                        <div className="shadow overflow-hidden sm:rounded-md">
                            <div className="px-4 py-5 bg-white sm:p-6">
                                <div className="grid grid-cols-6 gap-6">
                                    <div className="col-span-6 sm:col-span-3">
                                        <label htmlFor="robotType" className="block text-sm font-medium text-gray-700">
                                            Type of your robot
                                        </label>
                                        <AsyncSelect
                                            cacheOptions
                                            defaultOptions
                                            loadOptions={getRobotTypes}
                                            onChange={handleRobotTypeChange}/>
                                    </div>
 
                                    <div className="col-span-6 sm:col-spa n-*3">
                                        <label htmlFor="price" className="block text-sm font-medium text-gray-700">
                                            Price
                                        </label>
                                        <input
                                            type="number"
                                            name="price"
                                            id="price"
                                            autoComplete="price"
                                            placeholder="120000€"
                                            className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                            onChange={e => setPrice(e.target.value)}
                                        />
                                    </div>

                                    <div className="col-span-6">
                                        <label htmlFor="description"
                                               className="block text-sm font-medium text-gray-700">
                                            Description
                                        </label>
                                        <textarea
                                            name="description"
                                            id="description"
                                            autoComplete="description"
                                            placeholder="Announce description"
                                            className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                            onChange={e => setDescription(e.target.value)}
                                        />
                                    </div>

                                </div>
                            </div>
                            <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button
                                    type="submit"
                                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                >
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateRobot;