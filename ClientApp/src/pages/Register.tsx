﻿import {LockClosedIcon} from '@heroicons/react/solid'
import {SyntheticEvent, useState} from "react";
import {Redirect} from 'react-router-dom';

const Login = (props: { setLogged: (logged: boolean) => void }) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    
    const [redirect, setRedirect] = useState(false);
    
    const [error, setError] = useState('');

    // Function to register user
    const register = async (e: SyntheticEvent) => {
        e.preventDefault();

        if (password === confirmPassword) {
            try {
                const response = await fetch('https://localhost:8001/api/User/register', {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    credentials: "include",
                    body: JSON.stringify({
                        firstName,
                        lastName,
                        email,
                        password,
                        "RoleId": 1
                    })
                });

                const content = await response.json();

                if (response.ok && content.success === true) {
                    props.setLogged(true);
                    setRedirect(true);
                } else if (content.errors != null && content.errors.Email != null) {
                    setError(content.errors.Email);
                }
            }
            catch (e) {
                setError('An error as occured...');   
            }
        }
        else {
            setError('Passwords does not match');
        }
    }

    // If user's account is created successfully, he is redirected to main page
    if (redirect) {
        return <Redirect to={"/"}/>;
    }

    return (
        <div className="px-10 flex items-center justify-center filter drop-shadow-2xl px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Create your account</h2>
                    <h4 className="mt-3 text-center text-xl font-bold text-gray-600">And buy your new robot</h4>
                </div>
                <div className="p-2 flex justify-center text-red-500">
                    <span>{ error }</span>
                </div>
                <form className="mt-8 space-y-6" method="POST" onSubmit={register}>
                    <div>
                        <label htmlFor="firstName" className="sr-only">
                            First name
                        </label>
                        <input
                            id="firstName"
                            name="firstName"
                            type="text"
                            autoComplete="firstName"
                            required
                            className="appearance-none rounded relative block w-full px-3 py-2 my-6 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                            placeholder="First Name"
                            onChange={e => setFirstName(e.target.value)}
                        />
                    </div>
                    <div>
                        <label htmlFor="lastName" className="sr-only">
                            Last name
                        </label>
                        <input
                            id="lastName"
                            name="lastName"
                            type="text"
                            autoComplete="lastName"
                            required
                            className="appearance-none rounded relative block w-full px-3 py-2 my-6 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                            placeholder="Last Name"
                            onChange={e => setLastName(e.target.value)}
                        />
                    </div>
                    <div>
                        <label htmlFor="email" className="sr-only">
                            Email
                        </label>
                        <input
                            id="email"
                            name="email"
                            type="email"
                            autoComplete="email"
                            required
                            className="appearance-none rounded relative block w-full px-3 py-2 my-6 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                            placeholder="Email address"
                            onChange={e => setEmail(e.target.value)}
                        />
                    </div>
                    <div>
                        <label htmlFor="password" className="sr-only">
                            Password
                        </label>
                        <input
                            id="password"
                            name="password"
                            type="password"
                            autoComplete="current-password"
                            required
                            className="appearance-none rounded relative block w-full px-3 py-2 my-6 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                            placeholder="Password"
                            onChange={e => setPassword(e.target.value)}
                        />
                    </div>
                    <div>
                        <label htmlFor="confirmPassword" className="sr-only">
                            Confirm Password
                        </label>
                        <input
                            id="confirmPassword"
                            name="confirmPassword"
                            type="password"
                            autoComplete="current-password"
                            required
                            className="appearance-none rounded relative block w-full px-3 py-2 my-6 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                            placeholder="Confirm Password"
                            onChange={e => setConfirmPassword(e.target.value)}
                        />
                    </div>

                    <div>
                        <button
                            type="submit"
                            className="group relative w-full flex justify-center mt-20 py-2 my-6 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                        >
                            <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                                <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                                                aria-hidden="true"/>
                            </span>
                            Sign in
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Login;