﻿export type Robot = {
    id: number,
    price: number,
    description: string,
    robotTypeId: number,
    robotType: {
        id: number,
        type: string
    },
    sellerId: number,
    seller: {
        id: number,
        firstName: string,
        lastName: string,
        email: string,
        roleId: number,
        role: null
    },
    buyerId: null,
    buyer: null,
    createdAt: string,
    soldAt: null
};