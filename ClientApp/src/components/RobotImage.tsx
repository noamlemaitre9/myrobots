import React, {Fragment} from 'react'

export default function RobotImage() {
    return (
        <>
            <div className=""></div>
            <div className="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
                <svg
                    className="hidden lg:block absolute inset-y-0 h-full w-48 text-white transform -translate-x-1/4"
                    fill="currentColor"
                    viewBox="0 0 110 110"
                    preserveAspectRatio="none"
                    aria-hidden="true"
                >
                    <polygon points="50,0 110,0 60,110 0,110"/>
                </svg>
                <img
                    className="md:pl-10 h-56 shadow-inner md:shadow-none w-full object-cover sm:h-72 md:h-96 lg:w-full lg:h-full"
                    src="/images/robot.png"
                    alt=""
                />
            </div>
        </>
    )
}
