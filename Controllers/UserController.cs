using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyRobots.Data;
using MyRobots.Dtos;
using MyRobots.Helpers;
using MyRobots.Models;

namespace MyRobots.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly JwtService _jwtService;

        public UserController(ApplicationDbContext context, JwtService jwtService)
        {
            _context = context;
            _jwtService = jwtService;
        }
        
        // GET: api/User
        [HttpGet]
        public async Task<List<User>> GetUsers()
        {
            return await _context.Users.Include(x => x.Role).ToListAsync();
        }

        // GET: api/User/info
        [HttpGet("Info")]
        public async Task<ActionResult<User>> GetUserInfo()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);

                int userId = int.Parse(token.Issuer);
            
                var user = _context.Users.Include(x => x.Role).SingleOrDefault(x => x.Id == userId);

                if (user == null)
                {
                    return NotFound();
                }

                return user;
            }
            catch (Exception e)
            {
                return Unauthorized();
            }
        }

        // POST: api/User/register
        [HttpPost("register")]
        public async Task<ActionResult> Register(RegisterDto dto)
        {
            var user = new User
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                Password = BCrypt.Net.BCrypt.HashPassword(dto.Password),
                RoleId = dto.RoleId
            };
            
            if (ModelState.IsValid)  
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();

                Login(new LoginDto(user.Email, user.Password));

                return CreatedAtAction("GetUserInfo", new { id = user.Id }, user);
            }
            else
            {
                return ValidationProblem();
            }
        }
        
        // POST: api/User/login
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto dto)
        {
            User user = _context.Users.FirstOrDefault(user => user.Email == dto.Email);

            if (user == null || !BCrypt.Net.BCrypt.Verify(dto.Password, user.Password))
            {
                return BadRequest(new {message = "Bad Credentials"});
            }

            var jwt = _jwtService.Generate(user.Id);
            
            Response.Cookies.Append("jwt", jwt, new CookieOptions
            {
                HttpOnly = true,
                SameSite = SameSiteMode.None,
                Secure = true
            });

            return Ok(new
            {
                success = true,
                user = user
            });
        }
        
        // POST: api/User/Logout
        [HttpPost("Logout")]
        public async Task<IActionResult> Logout()
        {
            Response.Cookies.Delete("jwt", new CookieOptions
            {
                HttpOnly = true,
                SameSite = SameSiteMode.None,
                Secure = true
            });

            return Ok(new
            {
                message = "success"
            });
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
