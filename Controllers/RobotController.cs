using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyRobots.Data;
using MyRobots.Dtos;
using MyRobots.Helpers;
using MyRobots.Models;

namespace MyRobots.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RobotController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly JwtService _jwtService;

        public RobotController(ApplicationDbContext context, JwtService jwtService)
        {
            _context = context;
            _jwtService = jwtService;
        }

        // GET: api/robot
        [HttpGet]
        public async Task<List<Robot>> GetRobots()
        {
            return await _context.Robots
                .Include(x => x.RobotType)
                .Include(x => x.Seller)
                .Include(x => x.Buyer)
                .ToListAsync();
        }

        // GET: api/robot/info
        [HttpGet("info")]
        public async Task<ActionResult<Robot>> GetRobotInfo(int robotId)
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);

                var robot = _context.Robots.Include(x => x.RobotType).SingleOrDefault(x => x.Id == robotId);

                if (robot == null)
                {
                    return NotFound();
                }

                return robot;
            }
            catch (Exception e)
            {
                return Unauthorized();
            }
        }

        // POST: api/robot
        [HttpPost]
        public async Task<ActionResult> Store(StoreRobotDto dto)
        {
            var robot = new Robot
            {
                Price = dto.Price,
                Description = dto.Description,
                RobotTypeId = dto.RobotTypeId,
                SellerId = dto.SellerId,
                BuyerId = null
            };

            _context.Robots.Add(robot);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRobotInfo", new {id = robot.Id}, robot);
        }

        // PUT: api/robot/<id>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/robot/<id>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}