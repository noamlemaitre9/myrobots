using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyRobots.Data;
using MyRobots.Models;

namespace MyRobots.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RobotTypeController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RobotTypeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/RobotType
        [HttpGet]
        public async Task<List<RobotType>> GetRobotTypes()
        {
            return await _context.RobotTypes.ToListAsync();
        }

        // GET: api/RobotType/<id>
        [HttpGet("{id}")]
        public async Task<ActionResult<RobotType>> GetRobotType(int id)
        {
            var robotType = await _context.RobotTypes.FindAsync(id);

            if (robotType == null)
            {
                return NotFound();
            }

            return robotType;
        }

        // PUT: api/RobotType/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRobotType(int id, RobotType robotType)
        {
            if (id != robotType.Id)
            {
                return BadRequest();
            }

            _context.Entry(robotType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RobotTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RobotType
        [HttpPost]
        public async Task<ActionResult<RobotType>> StoreRobotType(RobotType robotType)
        {
            _context.RobotTypes.Add(robotType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRobotType", new { id = robotType.Id }, robotType);
        }

        // DELETE: api/RobotType/<id>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRobotType(int id)
        {
            var robotType = await _context.RobotTypes.FindAsync(id);
            if (robotType == null)
            {
                return NotFound();
            }

            _context.RobotTypes.Remove(robotType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool RobotTypeExists(int id)
        {
            return _context.RobotTypes.Any(e => e.Id == id);
        }
    }
}
