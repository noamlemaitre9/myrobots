﻿using System.ComponentModel.DataAnnotations;
using MyRobots.CustomValidation;

namespace MyRobots.Dtos
{
    public class StoreRobotDto
    {
        public float Price { get; set; }
        public string Description { get; set; }
        public int RobotTypeId { get; set; }
        public int SellerId { get; set; }
        public string Password { get; set; }
    }
}