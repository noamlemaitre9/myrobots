﻿using System.ComponentModel.DataAnnotations;
using MyRobots.CustomValidation;

namespace MyRobots.Dtos
{
    public class RegisterDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        [EmailAddress]
        [EmailUserUnique]
        public string Email { get; set; }
        public string Password { get; set; }
        
        public int RoleId { get; set; }
    }
}